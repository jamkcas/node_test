YUI().use('node', 'io', 'json', function(Y) {
  var list = Y.one('.itemList');

  var removeMessage = function removeMessage() {
    if(Y.one('.message')) Y.one('.message').remove();
  };

  var displayMessage = function displayMessage(msg) {
    removeMessage();

    list.prepend('<p class="message">' + msg + '</p>');

    setTimeout(removeMessage, 3000);
  };

  Y.one('#submit').on('click', function(e) {
    e.preventDefault();

    Y.io('/items', {
      data: {
        title: Y.one('#title')._node.value,
        description: Y.one('#description')._node.value
      },
      method: 'POST',
      on: {
        complete: function(id, response) {
          var res = Y.JSON.parse(response.response);
          if(res.status === 'error') {
            console.log(res.err);
          } else {
            list.prepend('<li data-id="' + res.item._id + '"><input type="text" class="itemTitle itemInput", value="' + res.item.title + '" /><input type="text" class="itemDescription itemInput", value="' + res.item.description + '" /><p class="edit">update</p><p class="delete">delete</p></li>');
          }
        }
      }
    });
  });

  list.delegate('click', function() {
    Y.io('/items/' + this.get('parentNode').getData('id'), {
      data: {
        title: this.previous('.itemTitle')._node.value,
        description: this.previous('.itemDescription')._node.value
      },
      method: 'PUT',
      on: {
        complete: function(id, response) {
          var res = Y.JSON.parse(response.response);
          if(res.status === 'error') {
            console.log(res.err);
          } else {
            displayMessage(res.msg);
          }
        }
      }
    });
  }, '.edit');

  list.delegate('click', function() {
    var parent = this.get('parentNode');

    Y.io('/items/' + parent.getData('id'), {
      method: 'DELETE',
      on: {
        complete: function(id, response) {
          var res = Y.JSON.parse(response.response);
          if(res.status === 'error') {
            console.log(res.err);
          } else {
            displayMessage(res.msg);
            parent.remove('destroy');
          }
        }
      }
    });
  }, '.delete');
});

