var express = require('express');
var mongoose = require('mongoose');

var router = express.Router();

var Schema = mongoose.Schema;

var Test = new Schema({
  title: {type: String, required: true},
  description: String
});

var TestModel = mongoose.model('Test', Test);

/* GET home page. */
router.get('/', function(req, res) {
  TestModel.find(function(err, items) {
    if(err) {
      console.log(err);
    } else {
      res.render('index', { title: 'To Do', items: items.reverse() });
    }
  });
});

router.post('/items', function(req, res) {
  var item = new TestModel({title: req.body.title, description: req.body.description});
  item.save(function(err) {
    if(err) {
      return res.send({status: 'error', err: err});
    } else {
      return res.json({status: 'success', item: item});
    }
  });
});

router.put('/items/:id', function(req, res) {
  var body = req.body;
  var id = req.params.id.replace(/\"/g, '');

  TestModel.findById(id, function(err, item) {
    item.title = body.title;
    item.description = body.description;

    return item.save(function(err) {
      if(err) {
        return res.send({status: 'error', err: err});
      } else {
        return res.send({status: 'success', msg: 'Item updated!'});
      }
    });
  });
});

router.delete('/items/:id', function(req, res) {
  var id = req.params.id.replace(/\"/g, '');

  TestModel.findById(id, function(err, item) {
    return item.remove(function(err) {
      if(err) {
        return res.send({status: 'error', err: err});
      } else {
        return res.send({status: 'success', msg: 'Item deleted!'});
      }
    });
  });
});

module.exports = router;
