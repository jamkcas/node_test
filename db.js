var common = require('./routes/common');
var config = common.config();
var mongoose = require('mongoose');

mongoose.connect('mongodb://' + config.mongo_db_name + ':' + config.mongo_db_password + '@ds053449.mongolab.com:53449/todo');

module.exports = mongoose.connection;